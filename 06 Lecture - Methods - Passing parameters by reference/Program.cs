﻿// See https://aka.ms/new-console-template for more information

namespace Method_2
{
    // Passing parameters by reference with the ref modifier
    class Program
    {
        static void Sum1(ref int x, int y)
        {
            x += y;
            Console.WriteLine($"The value in the method: {x}");
        }

        static void Main(string[] args)
        {
            // Argument, passed to the ref parameter must be initialized

            int x = 10;
            int y = 20;

            Sum1(ref x, y); // method call

            Console.WriteLine($"The value after the method: {x}");
        }
    }
}