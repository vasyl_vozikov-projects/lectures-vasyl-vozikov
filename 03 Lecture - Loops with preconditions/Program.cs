﻿// See https://aka.ms/new-console-template for more information

int x = 1;

while (x <= 10)
{
    Console.WriteLine(x);

    x = x + 1;
}

// Loop for

for (x = 1; x <= 20; x = x + 1)
{
    // Skip elements
    if (x == 6 || x == 7) continue;

    // Loop break 
    if (x == 10) break;
    Console.Write(x);
}

// x = x + 1 ~ x += 1 ~ x++ - increment
//                      x-- - decrement
