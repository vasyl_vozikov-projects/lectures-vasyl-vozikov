﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_Lecture___Working_with_file_system___Working_with_files
{
    class Program
    {
        static void Main()
        {
            if (File.Exists(@"C:\Demo\Myfile.txt"))
            {
                File.Delete(@"C:\Demo\Myfile.txt");
            }

            File.AppendAllText(@"C:\Demo\Myfile.txt", "I like C#. ");
            File.AppendAllText(@"C:\Demo\Myfile.txt", "Let's like it together");

            if (File.Exists(@"C:\Demo\Copyfile.txt"))
            {
                File.Delete(@"C:\Demo\Copyfile.txt");
            }
            File.Copy(@"C:\Demo\Myfile.txt", @"C:\Demo\Copyfile.txt");            

            string s = File.ReadAllText(@"C:\Demo\Copyfile.txt");
            Console.WriteLine(s);
        }
    }
}
