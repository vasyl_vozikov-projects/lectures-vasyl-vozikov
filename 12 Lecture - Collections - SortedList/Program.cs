﻿using System.Collections;   // To work with SortedList, System.Collections namespace must be used
class Program
{
    static void Main()
    {
        SortedList slColors = new SortedList();
        slColors.Add("w", "white");             // Creating an object of any type,  
        slColors.Add("b", "blue");              // with standard properties .Key and .Value
        slColors.Add("g", "green");
        slColors.Add("y", "yellow");

        foreach (DictionaryEntry x in slColors)
        {
            Console.WriteLine($"{x.Key}  {x.Value}");
        }
    }
}