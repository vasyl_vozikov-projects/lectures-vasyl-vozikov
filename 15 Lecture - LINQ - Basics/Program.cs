﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

// LINQ - Language INtegrated Query

int[] ints = { -2, 4, 8, 11, 9, -8, 14, 22, 5 };

// Count even positive elements, which are smaller than 10
int count = 0;
foreach (int i in ints)
{
    if (i > 0)
    {
        if (i % 2 == 0)
        {
            if (i < 10)
            {
                count++;
            }
        }
    }
}
Console.WriteLine(count);

// Using LINQ syntax of queries
count = (from n in ints
         where n > 0
         where n % 2 == 0
         where n < 10
         select n).Count();
Console.WriteLine(count);

// Using LINQ syntax of methods
count = ints
    .Where(n => n > 0)
    .Where(n => n % 2 == 0)
    .Count(n => n < 10);
Console.WriteLine(count);
Console.WriteLine();

int[] numbers = { 1, 2, 3, 4 };

var query = from n in numbers       // Iterating elements of array
            where n % 2 == 0        // Checking if elements are even
            select n * 2;           // Returning even element multiplied by 2
foreach (int i in query)
{
    Console.Write($"{i}  ");
}
Console.WriteLine();