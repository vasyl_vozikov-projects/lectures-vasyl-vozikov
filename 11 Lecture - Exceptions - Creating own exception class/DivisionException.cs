﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_Lecture___Exceptions___Creating_own_exception_class
{
    [Serializable]
    public class DivisionException : Exception
    {
        public int X { get; set; }
        public int Y { get; set; }
        public DivisionException(int x, int y)
        {
            X = x;
            Y = y;
        }
        public override string Message
        {
            get
            {
                return "Invalid division operation!";
            }
        }

        // To satisfy Sonar, 4 additional constructors must be implemented
        public DivisionException() { }
        public DivisionException(string message) : base(message) { }
        public DivisionException(string message, Exception innerException) : base(message, innerException) { }
        protected DivisionException(System.Runtime.Serialization.SerializationInfo info, System.Runtime.Serialization.StreamingContext context) : base(info, context) { }
    }
}
