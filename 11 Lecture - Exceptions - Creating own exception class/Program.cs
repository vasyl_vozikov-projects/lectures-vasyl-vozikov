﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _11_Lecture___Exceptions___Creating_own_exception_class
{
    class Program
    {
        static void Main()
        {
            try
            {
                Console.WriteLine("Enter first integer number");
                int firstValue = int.Parse(Console.ReadLine());

                Console.WriteLine("Enter second integer number");
                int secondValue = int.Parse(Console.ReadLine());

                if (firstValue % secondValue != 0 || secondValue == 0)      // Filter of exceptions
                {
                    // Generating own exception
                    throw new DivisionException(firstValue, secondValue);   
                }
                int result = firstValue / secondValue;
                Console.WriteLine($"{firstValue} / {secondValue} = {result}");
            }
                    // Reacting to exception
            catch (DivisionException e) when (e.Y == 0)                     // Filter of exceptions
            {
                Console.WriteLine("Exception: {0}", e.Message);
                Console.WriteLine("Division by zero is forbidden!");
            }
            catch (DivisionException e)                                     
            {
                Console.WriteLine("Exception: {0}", e.Message);
                Console.WriteLine("Number {0} is not divisible by {1}", e.X, e.Y);
            }
            catch (Exception e)
            {
                Console.WriteLine("Exception: {0}", e.Message);
            }
        }
    }
}
