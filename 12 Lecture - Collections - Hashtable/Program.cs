﻿using System.Collections;   // To work with associative array, System.Collections namespace must be used
class Program
{
    static void Main()
    {
        Hashtable currencies = new Hashtable();        
        currencies.Add("US", "Dollar");             // Creating an object of any type,  
        currencies.Add("Japan", "Yen");             // with standard properties .Key and .Value
        currencies.Add("Europe", "Euro");
        currencies.Add("China", "Yuan");

        // To access an object of associative array, .Key property is used
        Console.WriteLine($"The US currency is {currencies["US"]}");

        foreach (DictionaryEntry currency in currencies)
        {
            Console.WriteLine($"{currency.Key}  {currency.Value}");
        }
    }
}