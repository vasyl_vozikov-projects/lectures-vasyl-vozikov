﻿using System.Collections.Generic;   // To work with Queue<T>, System.Collections.Generic namespace must be used
class Student
{
    public string name { get; }
    public Student(string name)
    {
        this.name = name;
    }
}

class Program
{
    static void Main()
    {
        // FIFO (First In First Out) is a principle, which is used when working with queues
        Queue<Student> queue= new Queue<Student>();     // Creating new empty typed queue of type Queue<T>
        queue.Enqueue(new Student("Vasyl Vozikov"));    
        queue.Enqueue(new Student("San Sanko"));        // Adding objects of one type to queue with
        queue.Enqueue(new Student("Ivan Ivanov"));      // standard method .Enqueue
        queue.Enqueue(new Student("Dan Danov"));

        Console.WriteLine($"First student in the queue is {queue.Peek()}");    // Standard method .Peek returns the first
                                                                               // element of the queue
        Console.WriteLine("The order of students in queue:");
        foreach (Student item in queue)
        {
            Console.WriteLine(item.name);
        }

        Console.WriteLine("Start interview");
        while (queue.Count > 0)
        {                                                
            // To delete an object from the queue, standard method .Dequeue is used
            Console.WriteLine($"{queue.Dequeue().name} successfully passed the interview!");     
        }
        Console.WriteLine($"Total students in the queue: {queue.Count}");
    }
}