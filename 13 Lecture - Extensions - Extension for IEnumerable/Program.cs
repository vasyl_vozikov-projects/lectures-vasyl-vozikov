﻿using System.Collections;

static class Extensions
{
    // Method PrintData() can be applied by any class or struct, which implements IEnumerable interface
    public static void PrintData(this IEnumerable iterator)
    {
        foreach (var item in iterator)
        {
            Console.Write(item + " ");
        }
    }
    // Method GetEveryNth() returns collection, which contains every n-th element of current collection
    public static IEnumerable GetEveryNth(this IEnumerable source, int n)
    {
        if (n <= 0)
        {
            throw new ArgumentOutOfRangeException();
        }

        int index = 1;
        foreach (var item in source)
        {
            if (index % n == 0)
            {
                yield return item;
            }
            index++;
        }
    }
}
class Program
{
    static void Main()
    {
        // Collections below implement IEnumerable interface,
        // so the PrintData() method is available for each of them
        string[] str = { "The", "quick", "brown", "fox", "jumped", "over", "the", "lazy", "dog" };
        str.PrintData();
        Console.WriteLine();
        // Getting every 3rd element of collection
        (str.GetEveryNth(3)).PrintData();
        Console.WriteLine();

        List<int> ints = new List<int> { 10, 20, 30, 40, 50, 60, 70 };
        ints.PrintData();
        Console.WriteLine();
        // Getting every 3rd element of collection
        (ints.GetEveryNth(3)).PrintData();
        Console.WriteLine();

        ArrayList list = new ArrayList() { 56, 'c', "EPAM", true };
        list.PrintData();
        Console.WriteLine();
    }
}