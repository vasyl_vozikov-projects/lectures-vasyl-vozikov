﻿    class Program
{
    static void Main()
    {
        // Standard delegate Func<T, TResult> accepts parameters of type T and returns type TResult
        Func<int, double> f1 = (x) => x / 2;
        Func<double, double> f2 = (x) => x / 2;
        Func<double, int> f3 = (x) => (int)x / 2;

        int n = 9;
        Console.WriteLine($"Result 1: {f1(n)}");
        Console.WriteLine($"Result 2: {f2(n)}");
        Console.WriteLine($"Result 3: {f3(n)}");

        Func<int, int, bool> f4 = (x, y) => x == y;
        int a = 4, b = 5, c = 4;
        if (f4(a, c))
        {
            Console.WriteLine("Numbers are equal");
        }
        else
        {
            Console.WriteLine("Numbers are not equal");
        }

        Func<int, int, int, int> max = (x, y, z) => Math.Max(Math.Max(x, y), z);
        Console.WriteLine("Max number is {0}", max(2, 8, 5));
    }
}