﻿// See https://aka.ms/new-console-template for more information

namespace Method_4
{
    // Passing parameters with the in modifier
    class Program
    {
        static void Sum(in int x,in int y)
        {
            Console.WriteLine($"The sum is {x + y}");
        }

        static void Main(string[] args)
        {
            int x = 10;
            int y = 20;

            Sum(in x, in y);
        }
    }
}
