﻿using System;
using System.IO;
using System.Runtime.Serialization.Json;
using System.Runtime.Serialization;

namespace _16_Lecture___Serialization___JSON_serialization
{
    [DataContract]
    public class Person
    {
        [DataMember]
        public string Name { get; set; }
        [DataMember]
        public int Age { get; set; }
        public Person(string name, int age)
        {
            Name = name;
            Age = age;
        }
    }
    class Program
    {
        static void Main()
        {
            Person person1 = new Person("Vasyl", 29);
            Person person2 = new Person("Sasha", 22);
            Person[] people = new Person[] { person1, person2 };

            DataContractJsonSerializer jsonSerializer = new DataContractJsonSerializer(typeof(Person[]));
            using (FileStream fs = new FileStream(@"C:\Demo\People.json", FileMode.OpenOrCreate))
            {
                jsonSerializer.WriteObject(fs, people);
            }

            using (FileStream fs = new FileStream(@"C:\Demo\People.json", FileMode.OpenOrCreate))
            {
                Person[] newpeople = (Person[])jsonSerializer.ReadObject(fs);
                foreach (Person p in newpeople)
                {
                    Console.WriteLine("{0}  {1}", p.Name, p.Age);
                }
            }
        }
    }
}
