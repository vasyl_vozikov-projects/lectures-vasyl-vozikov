﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Enter number");

int x = Convert.ToInt32(Console.ReadLine());

//Conditional operator. Full form

if (x % 2 == 0)
{
    Console.WriteLine($"Number {x} is even");
}
else
{
    Console.WriteLine($"Number {x} is odd");
}

//Conditional operator. Short form

if (x % 2 == 0)
{
    Console.WriteLine($"Number {x} is even");
}

if (x % 2 != 0)
{
    Console.WriteLine($"Number {x} is odd");
}

// Ternary Operator

string result = (x % 2 == 0) ? $"Number {x} is even" : $"Number {x} is odd";

Console.WriteLine(result);
