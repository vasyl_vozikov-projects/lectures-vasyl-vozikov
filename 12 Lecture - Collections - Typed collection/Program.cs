﻿using System.Collections.Generic;   // To work with List<T>, System.Collections.Generic namespace must be used
class Program
{
    static void Main()
    {
        List<string> list = new List<string>();     // Creating new empty typed collection of type List<T>
        list.Add("Bill Gates");     // Adding elements of one type to collection with standard method .Add
        list.Add("Steven Jobs");
        list.Add("Sergey Brin");
        list.Add("Mark Zuckerberg");

        Console.WriteLine("Founders of world's biggest IT companies:");
        foreach (string item in list)
        {
            Console.WriteLine(item);
        }
        Console.WriteLine("Founders of world's biggest IT companies:");
        for (int i = 0; i < list.Count; i++)
        {
            Console.WriteLine(list[i]);
        }
        Console.WriteLine($"The founder of Microsoft is {list[0]}");
    }
}
