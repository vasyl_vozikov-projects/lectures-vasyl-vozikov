﻿// See https://aka.ms/new-console-template for more information

// Entering number of rows and columns

Console.WriteLine("Enter number of rows");
int n = int.Parse(Console.ReadLine());

Console.WriteLine("Enter number of columns");
int m = int.Parse(Console.ReadLine());

// Declaring multi-dimensional array 

int[,] array = new int[n, m];

// Filling up the array

Console.WriteLine($"Enter {n * m} elements");

for (int i = 0; i < n; i++)
{
    for (int j = 0; j < m; j++)
    {
        array[i, j] = int.Parse(Console.ReadLine());
    }
}

// Display the array

Console.WriteLine("Source array");

for (int i = 0; i < n; i++)
{
    for (int j = 0; j < m; j++)
    {
        Console.Write($"{array[i,j]} ");
    }
    Console.WriteLine();
}

// Finding and displaying sum of all elements of the array

int sum = 0;

for (int i = 0; i < array.GetLength(0); i++) // GetLength is method 
{
    for (int j = 0; j < array.GetLength(1); j++)
    {
        sum += array[i, j];
    }
}

Console.WriteLine($"Sum of {n * m} elements of the array is {sum}");