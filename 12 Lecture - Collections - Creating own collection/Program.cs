﻿using System.Collections;
public class Student
{
    public string Name { get; }
    public int Rate { get; set; }
    public Student(string Name, int Rate)
    {
        this.Name = Name;
        this.Rate = Rate;
    }
    public override string ToString()
    {
        return String.Format($"{Name} has rate {Rate}");
    }
}
public class StudentCollection : IEnumerable
{
    private IList List { get; }
    public StudentCollection()
    {
        List = new ArrayList();
    }
    public int Count
    {
        get
        {
            return List.Count;
        }
    }
    public IEnumerator GetEnumerator()
    {
        for (int i = 0; i < List.Count; i++)
        {
            yield return List[i];
        }
    }
    public int Add(string name, int rate)
    {
        return List.Add(new Student(name, rate));
    }
    public int Add(Student student)
    {
        return List.Add(student);
    }
    public void RemoveAt(int index)
    {
        List.RemoveAt(index);
    }
    public void Remove(Student student)
    {
        List.Remove(student);
    }
    public Student this[int index]
    {
        get
        {
            return (Student)List[index];
        }
        set
        {
            List[index] = value;    
        }
    }
    public Student BestStudent()
    {
        int bestindex = 0;
        for (int i = 0; i < List.Count; i++)
        {
            if ((List[bestindex] as Student).Rate < (List[i] as Student).Rate)
            {
                bestindex = i;
            }
        }
        return List[bestindex] as Student;
    }
}
class Program
{
    static void Main()
    {
        StudentCollection group = new StudentCollection();

        group.Add("Steve Jobs", 98);
        group.Add("Larry Page", 90);
        group.Add("Bill Gates", 88);
        group.Add("Mark Zuckerberg", 99);

        group.Add(new Student("Sergey Brin", 92));

        foreach (var student in group)
        {
            Console.WriteLine(student);
        }
        Console.WriteLine();        

        group[2] = new Student("Vasyl Vozikov", 100);
        group.RemoveAt(1);
        group.Remove(group[3]);
        Console.WriteLine($"The best student in the group is {group.BestStudent()}");

        for (int i = 0; i < group.Count; i++)
        {
            Console.WriteLine(group[i]);
        }

    }
}