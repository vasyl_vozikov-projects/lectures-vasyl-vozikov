﻿// See https://aka.ms/new-console-template for more information

/*

// Random class fills the array with random numbers

Random r = new Random();
int[] array = new int[10];

for (int i = 0; i < array.Length; i++)
    array[i] = r.Next(0,11);

Console.WriteLine("Random array");
foreach (int x in array)
{
    Console.Write($"{x}  ");
}
Console.WriteLine();

// Method .Sum() finds sum of elements of array - doesnt work with empty array!

Console.WriteLine("Sum = {0}", array.Sum());

// Methods .Min() and .Max() - doesnt work with empty array!

Console.WriteLine("Min = {0}, Max = {1}", array.Min(), array.Max());

// .Reverse() method - doesnt work with empty array!

Array.Reverse(array);

Console.WriteLine("Reversed array");
foreach (int x in array)
{
    Console.Write($"{x}  ");
}
Console.WriteLine();

// .Sort() method - doesnt work with empty array!

Array.Sort(array);

Console.WriteLine("Sorted array");
foreach (int x in array)
{
    Console.Write($"{x}  ");
}
Console.WriteLine();

// Enter array elements (any number), separated by a space with .Split() method

Console.WriteLine("Enter array elements (any number), separated by a space");

string[] array2 = Console.ReadLine().Split(' ');
int[] array3 = new int[array2.Length];

for (int i = 0; i < array3.Length; i++)
    array3[i] = int.Parse(array2[i]);

int sum = 0;

foreach (int x in array3)
    sum += x;

Console.WriteLine($"Sum = {sum}");

// Aternative way of declaring arrays with Array class and .CreateInstance() method

Array mas = Array.CreateInstance(typeof(int), 5);

Console.WriteLine("Enter 5 array elements into a column");

for (int i = 0; i < mas.Length; i++)
    mas.SetValue(int.Parse(Console.ReadLine()), i);

foreach (int x in mas)
    Console.WriteLine(x);

// The same for multi-dimensional array

Array mas2 = Array.CreateInstance(typeof(int), 3,3);

Console.WriteLine("Enter 9 array elements into a column");

for (int i = 0; i < mas2.GetLength(0); i++)
    for (int j = 0; j < mas2.GetLength(1); j++)
        mas2.SetValue(int.Parse(Console.ReadLine()), i,j);

foreach (int x in mas2)
    Console.WriteLine(x);

sum = 0;

for (int i = 0; i < mas2.GetLength(0); i++)
    for (int j = 0; j < mas2.GetLength(1); j++)
        sum += Convert.ToInt32(mas2.GetValue(i, j));

Console.WriteLine($"Sum = {sum}");

int[,] mas3 = (int[,]) mas2;

sum = 0;

for (int i = 0; i < mas3.GetLength(0); i++)
    for (int j = 0; j < mas3.GetLength(1); j++)
        sum += Convert.ToInt32(mas3.GetValue(i, j));

*/

// Accessing elements from the end of array

int[] numbers = { 1, 2, 3, 4, 5 };

// Old solution

Console.WriteLine(numbers[numbers.Length - 1]);
Console.WriteLine(numbers[numbers.Length - 2]);
Console.WriteLine(numbers[numbers.Length - 3]);

// New solution

Console.WriteLine(numbers[^1]);
Console.WriteLine(numbers[^2]);
Console.WriteLine(numbers[^3]);

// Indexation

Index i1 = 3;  // Third element from the beginning
Index i2 = ^4; // Fourth element from the end

int[] a = { 1, 2, 3, 4, 5 };
Console.WriteLine($"{a[i1]}, {a[i2]}");

// Getting range of elements
/*
var slice = a[i1..i2];

foreach (var sl in slice)
{
    Console.WriteLine(sl);
}
*/
// Using range with strings

string input = "This is a test of Ranges!";
string output = input[^7..^1];

Console.WriteLine(output);

// Deleting elements

output = input[^7..]; // Equivalent of input[^7..^0]

Console.WriteLine(output);

output = input[..^1]; // Equivalent of input[^0..^1]

Console.WriteLine(output);

// Using range as variable

Range r = 0..^1;
output = input[r];

Console.WriteLine(output);