﻿// See https://aka.ms/new-console-template for more information

namespace Overlapped_Method
{
    class Person
    {
        public string firstname;
        public string lastname;
        public int age;
        public void AboutMe()
        {
            Console.WriteLine($"I'm {firstname} {lastname}. I'm {age} yo. ");
        }
    }
    class Student : Person
    {
        public string university;

        // Method overload
        public void AboutMe(string university)
        {
            Console.WriteLine($"I'm {firstname} {lastname}. I'm {age} yo. I'm a student of {university}");
        }

        // Method overlapping
        public new void AboutMe()
        {
            Console.WriteLine($"I'm {firstname} {lastname}. I'm {age} yo. I'm a student of {university}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person p = new Person();
            p.firstname = "Boris";
            p.lastname = "Johnson";
            p.age = 57;
            p.AboutMe();             // Parent method call

            Student s = new Student();
            s.firstname = "Vasyl";
            s.lastname = "Vozikov";
            s.age = 29;
            s.university = "EPAM";
            
            s.AboutMe();             // Overlapped method call
            s.AboutMe("PTU");        // Overloaded method call with parameter

            // Casting to parent class type
            ((Person)s).AboutMe();   // Parent method call
            (s as Person).AboutMe(); // Parent method call

            if (s is Person)         // "is" keyword checks whether variable s is an object of class Person
            {
                Console.WriteLine($"{s.firstname} {s.lastname} is a Person!");
            }
            else
            {
                Console.WriteLine($"{s.firstname} {s.lastname} is not a Person!");
            }

            if (p is Student)
            {
                Console.WriteLine($"{p.firstname} {p.lastname} is a Person!");
            }
            else
            {
                Console.WriteLine($"{p.firstname} {p.lastname} is not a Student!");
            }
        }
    }
}