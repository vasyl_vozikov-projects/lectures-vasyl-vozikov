﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_Lecture___Working_with_file_system___Working_with_class_FileInfo
{
    class Program
    {
        static void Main()
        {
            FileInfo data = new FileInfo(@"C:\Demo\Myfile.txt");
            data.CopyTo(@"C:\Demo\Copyfile1.txt");

            FileInfo copyFile = new FileInfo(@"C:\Demo\Copyfile1.txt");
            Console.WriteLine(copyFile.OpenText().ReadToEnd());

            copyFile.Attributes = FileAttributes.ReadOnly;
          //copyFile.Attributes = FileAttributes.Hidden;
            
            if (copyFile.Attributes == FileAttributes.ReadOnly)
            {
                Console.WriteLine("YES");
            }

            data.Delete();
        }
    }
}
