﻿// See https://aka.ms/new-console-template for more information

namespace OOP_1
{
    class Worker
    {
        private string firstname;
        private string lastname;
        private int salary;
        public static int total_salary;

        public Worker(string firstname, string lastname, int salary)
        {
            this.firstname = firstname;
            this.lastname = lastname;
            this.salary = salary;
            total_salary += salary;
        }
    }
    class Person
    {
        private string name;
        private int number;
        private static int total;

        // Static constructor
        static Person()
        {
            total = 1;
        }
        public Person(string name)
        {
            number = total;
            this.name = name;
            total++;
        }

        // Static property
        public static int Total
        {
            get
            {
                return total - 1;
            }
        }

        public string Name
        {
            get
            {
                return name;
            }
        }

        // Static method
        public static void AboutUs()
        {
            Console.WriteLine($"We are people, and there are {Total} of us");
        }

        public void AboutMe()
        {
            Console.WriteLine($"My name is {name} and my number is {number}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Worker w1 = new Worker("Mark", "Zukerberg", 3000000);
            Worker w2 = new Worker("Bill", "Gates", 2500000);
            Console.WriteLine($"Total salary: {Worker.total_salary}");

            Person p1 = new Person("Ivan");
            p1.AboutMe();

            Person p2 = new Person("Sasha");
            p2.AboutMe();

            Person.AboutUs();
        }
    }
}