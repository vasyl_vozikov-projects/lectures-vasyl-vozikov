﻿using System;
using System.IO;
using System.Xml;
using System.Xml.Serialization;

namespace _16_Lecture___Serialization___XML_serialization
{
    public class Employee
    {
        public string Name;
        public int Age;
    }
    public class Company
    {
        [XmlAttribute]
        public string CompanyName;
        public Employee[] Employees;
    }
    class Program
    {
        static void Main()
        {
            string fileName = @"C:\Demo\Company.xml";
            XmlSerializer serializer = new XmlSerializer(typeof(Company));
            TextWriter writer = new StreamWriter(fileName);

            Company company = new Company();
            company.CompanyName = "ABC Infosystems";
            Employee emp1 = new Employee();
            Employee emp2 = new Employee();
            emp1.Name = "Vasyl";
            emp1.Age = 29;
            emp2.Name = "Sasha";
            emp2.Age = 22;
            company.Employees = new Employee[2] {emp1, emp2 };

            serializer.Serialize(writer, company);
            writer.Close();

            FileStream fs = new FileStream(fileName, FileMode.Open);
            Company company1 = (Company)serializer.Deserialize(fs);

            Console.WriteLine(company1.CompanyName);
            foreach (Employee emp in company1.Employees)
            {
                Console.WriteLine("{0}  {1}", emp.Name, emp.Age);
            }
        }
    }
}
