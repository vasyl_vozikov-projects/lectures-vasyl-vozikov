﻿static class StringExtension
{
    // Extension method for counting words in a sentence
    public static int WordCounter(this String str)
    {
        string[] words = str.Split( new char[] { ' ', ',', '.'}, StringSplitOptions.RemoveEmptyEntries);
        return words.Length;
    }
    // Extension method for counting specific charachters in a sentence
    public static int LetterCounter(this String str, char letter)
    {
        int counter = 0;
        for (int i = 0; i < str.Length; i++)
        {
            if (str[i] == letter)
            {
                counter++;
            }
        }
        return counter;
    }
}
class Program
{
    static void Main()
    {
        string s = "EPAM is an American company, that specializes on developing products and bla bla bla";
        Console.WriteLine($"String s contains {s.WordCounter()} words");
        Console.WriteLine($"String s contains {s.LetterCounter(' ')} letters e");
    }
}