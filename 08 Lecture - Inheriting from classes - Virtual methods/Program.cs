﻿// See https://aka.ms/new-console-template for more information

namespace Virtual_Method
{
    class Person
    {
        public string firstname;
        public string lastname;
        public virtual void AboutMe()
        {
            Console.WriteLine($"My name is {firstname} {lastname}");
        }

        // ToString() method override
        public override string ToString()
        {
            return string.Format($"{firstname} {lastname}");
        }
    }
    class Student : Person
    {
        public string university;

        // Virtual method override
        public override void AboutMe()
        {
            Console.WriteLine($"My name is {firstname} {lastname}, student of {university}");
        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person person = new Person();
            person.firstname = "Sasha";
            person.lastname = "Sabad";
            person.AboutMe();               // Parent method call
            Console.WriteLine(person);

            Student student = new Student();
            student.firstname = "Vasyl";
            student.lastname = "Vozikov";
            student.university = "EPAM";
            student.AboutMe();              // Overrided method call
            (student as Person).AboutMe();  // Trying to call parent method. Result is overrided method
        }
    }
}