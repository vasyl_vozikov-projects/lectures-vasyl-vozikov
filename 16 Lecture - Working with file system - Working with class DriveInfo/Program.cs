﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_Lecture___Working_with_file_system___Working_with_class_Directory
{
    class Program
    {
        static void Main()
        {
            DriveInfo[] drives = DriveInfo.GetDrives();

            foreach (DriveInfo drive in drives)
            {
                Console.WriteLine("Drive {0}", drive.Name);
                Console.WriteLine(" File type: {0}", drive.DriveType);
                if (drive.IsReady)
                {
                    Console.WriteLine(" Volume label: {0}", drive.VolumeLabel);
                    Console.WriteLine(" File system: {0}", drive.DriveFormat);
                    Console.WriteLine(" Total size: {0} MB", drive.TotalSize / 1000000);
                    Console.WriteLine(" Free space: {0} MB", drive.TotalFreeSpace / 1000000);
                }
                else
                {
                    Console.WriteLine("No media available..");
                }
                Console.WriteLine();
            }
        }
    }
}
