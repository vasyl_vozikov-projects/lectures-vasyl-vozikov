﻿static class EnumExtension
{
    public static bool IsColdColor(this ConsoleColor color)
    {
        if (color is ConsoleColor.Blue || color is ConsoleColor.Green)
        {
            return true;
        }
        return false;
    }
}
class Program
{
    static void Main()
    {
        ConsoleColor[] colores = { ConsoleColor.Blue, ConsoleColor.Red };
        foreach (ConsoleColor color in colores)
        {
            if (color.IsColdColor())
            {
                Console.WriteLine($"{color} is cold color");
            }
            else
            {
                Console.WriteLine($"{color} is warm color");
            }
        }
    }
}