﻿// See https://aka.ms/new-console-template for more information

namespace OOP_2
{
    class Person
    {
        public string FirstName;
        public string LastName;
        public int Age;

        // Method for setting a value of the "FirstName" field
        public void SetFirstName(string FirstName)
        {
            this.FirstName = FirstName;
        }

        // Method for getting a value from the "FirstName" field
        public string GetFirstName()
        {
            return FirstName;
        }

        public void SetLastName(string surname)
        {
            LastName = surname;
        }

        public string GetLastName()
        {
            return LastName;
        }

        public void SetAge(int Age)
        {
            if (Age > 0)
            {
                this.Age = Age;
            }
            else
            {
                Console.WriteLine("Error!");
            }
        }

        public int GetAge()
        {
            return Age;
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person();

            // Using methods for setting a value instead of assigning
            p1.SetFirstName("Vasyl");
            p1.SetLastName("Vozikov");
            p1.SetAge(29);

            Console.WriteLine($"Firstname: {p1.GetFirstName()}, Lastname: {p1.GetLastName()}, Age: {p1.GetAge()}");

            // After one year...
            p1.SetAge(30);

            Console.WriteLine($"Firstname: {p1.GetFirstName()}, Lastname: {p1.GetLastName()}, Age: {p1.GetAge()}");
        }
    }
}
