﻿// See https://aka.ms/new-console-template for more information

// Declaring array

int[][] array = new int[3][];

array[0] = new int[2];
array[1] = new int[4];
array[2] = new int[3];

// Filling the array

for (int i = 0; i < 3; i++)
{
    Console.WriteLine("Enter {0} row", i + 1);

    for (int j = 0; j < array[i].Length; j++)
    {
        array[i][j] = int.Parse(Console.ReadLine());
    }
}

// Displaying array

Console.WriteLine("Jagged array:");

for (int i = 0; i < 3; i++)
{
    for (int j = 0; j < array[i].Length; j++)
    {
        Console.Write($"{array[i][j]} ");
    }
    Console.WriteLine();
}

// Finding and displaying sum of all elements of the array

int sum = 0;

for (int i = 0; i < 3; i++) // GetLength is method 
{
    for (int j = 0; j < array[i].Length; j++)
    {
        sum += array[i][j];
    }
}

Console.WriteLine($"Sum of elements of the array is {sum}");

// Finding and displaying sum of all elements of the array wit foreach loop

sum = 0;

foreach (int[] y  in array)
{
    foreach (int x in y)
    {
        sum += x;
    }
}

Console.WriteLine($"Sum of elements of the array is {sum}");