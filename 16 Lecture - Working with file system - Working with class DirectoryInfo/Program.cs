﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_Lecture___Working_with_file_system___Working_with_class_Directory
{
    class Program
    {
        static void Main()
        {
            DirectoryInfo backup = new DirectoryInfo(@"C:\Demo\Backup");
            if (!backup.Exists)
            {
                backup.Create();
            }

            string currentFokderName = Directory.GetCurrentDirectory();
            DirectoryInfo currentFolder = new DirectoryInfo(currentFokderName);

            foreach (FileInfo file in currentFolder.GetFiles())
            {
                file.CopyTo(backup.FullName + @"\" + file.Name);
            }
        }
    }
}
