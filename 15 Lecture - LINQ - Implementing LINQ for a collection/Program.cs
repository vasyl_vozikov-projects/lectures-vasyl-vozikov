﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15_Lecture___LINQ___Implementing_LINQ_for_a_collection
{
    public class Employee
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Salary { get; set; }
    }
    class Program
    {
        static void Main()
        {
            var employees = new List<Employee>()
            {
                new Employee{FirstName = "Petro", LastName = "Petrenko", Salary = 450},
                new Employee{FirstName = "Dmytro", LastName = "Dmytrenko", Salary = 900},
                new Employee{FirstName = "Anton", LastName = "Antonenko", Salary = 850},
                new Employee{FirstName = "Bohdan", LastName = "Dmytrenko", Salary = 900}
            };

            // Sorting list of employees
            var query1 = from employee in employees
                        let fullname = employee.LastName + " " + employee.FirstName
                        orderby fullname        // Sorting order
                        select fullname;
            Console.WriteLine("Displaying sorted list:");
            foreach (var q in query1)
            {
                Console.WriteLine(q);
            }
            Console.WriteLine();

            // Displaying lastname and initials
            var query2 = employees.Select(emp => emp.LastName + " " + emp.FirstName.First() + ".");          // .Select() returns all elements
            Console.WriteLine("Displaying lastname and initials:");
            foreach (var q in query2)
            {
                Console.WriteLine(q);
            }
            Console.WriteLine();

            // Displaying employees, whose first and last names start from the same letter
            var query3 = employees.Where(emp => emp.LastName.First().Equals(emp.FirstName.First()));    // .Where() returns elements after checking
            Console.WriteLine("Displaying employees, whose first and last names start from the same letter:");
            foreach (var q in query3)
            {
                Console.WriteLine($"{q.LastName} {q.FirstName}");
            }
            Console.WriteLine();

            // Displaying employees, whose salary is greater than 800
            var query4 = from emp in employees
                         where emp.Salary > 800
                         orderby emp.LastName, emp.FirstName
                         select emp;
            Console.WriteLine("Displaying employees, whose salary is greater than 800:");
            foreach (var q in query4)
            {
                Console.WriteLine($"{q.LastName} {q.FirstName} earns {q.Salary}");
            }
            Console.WriteLine();

            // Displaying employees, sorted by salary 
            var query5 = from emp in employees
                         orderby emp.Salary,
                                 emp.LastName descending,
                                 emp.FirstName descending
                         select emp;
            Console.WriteLine("Displaying employees, sorted by salary:");
            foreach (var q in query5)
            {
                Console.WriteLine($"{q.LastName} {q.FirstName}, {q.Salary}");
            }
            Console.WriteLine();

            // Grouping employees
            var query6 = from emp in employees
                         group emp by new
                         {
                             emp.Salary,
                             emp.LastName
                         };
            Console.WriteLine("Grouping employees:");
            foreach (var group in query6)
            {
                Console.WriteLine(group.Key);
                foreach (var g in group)
                {
                    Console.WriteLine(g.FirstName);
                }
            }
            Console.WriteLine();

            // Mixed syntax of queries

            // Displaying first employee of sorted list
            Console.WriteLine("Displaying first element of sorted list:");
            var name = (from emp in employees
                        orderby emp.LastName
                        select emp).First().LastName;
            Console.WriteLine(name);
            Console.WriteLine();

            // Counting employees whose first name contains specified character
            Console.WriteLine("Counting employees whose first name contains specified character:");
            var queryX = (from emp in employees
                          where emp.FirstName.Contains('n')
                          select emp).Count();
            Console.WriteLine(queryX);
            Console.WriteLine();
        }
    }
}
