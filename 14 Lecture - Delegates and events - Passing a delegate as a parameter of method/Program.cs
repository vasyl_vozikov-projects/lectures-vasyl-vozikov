﻿delegate double Func(double x);
class Program
{    
    static void Table(Func y, double x1, double x2, double h)
    {
        double x = x1;

        Console.WriteLine(" ----- x ----- y(x) ----");
        while (x <= x2)
        {
            Console.WriteLine(" | {0,7:0.000} | {1,7:0.000}   |", x, y(x));
            x += h;
        }
        Console.WriteLine(" -----------------------");
    }
    static double Sqr(double x)
    {
        return x * x;
    }
    static void Main()
    {
        Console.WriteLine("Sin function value table");
        Table(Math.Sin, -2, 2, 1);

        Console.WriteLine("Cos function value table");
        Table(Math.Cos, -2, 2, 1);

        Console.WriteLine("Sqr function value table");
        Table(Sqr, -2, 2, 1);

        Console.WriteLine("Sqrt function value table");
        Table(Math.Sqrt, -2, 2, 1);

        Console.WriteLine("Anonymus function value table");
        Table(delegate(double x) { return 2 * x; }, -2, 2, 1);  // Passing anonymus method to a patameter

        Console.WriteLine("Anonymus function value table");
        Table(x => 2 * x, 0, 4, 1);                             // Passing lambda expression to a patameter
    }
}
