﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Lecture___Aggregation
{
    class Room
    {
        private double length;
        public double width;

        public Room(double length, double width)
        {
            this.length = length;
            this.width = width;
        }
        public double GetAreaRoom()
        {
            return length * width;
        }
    }

}
