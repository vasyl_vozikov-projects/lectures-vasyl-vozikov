﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Lecture___Aggregation
{
    class House
    {
        Room bedroom;
        Room kitchen;
        Color colorRoof;
        public House(double length1, double width1, double length2, double width2, Color color)
        {
            bedroom = new Room(length1, width1);
            kitchen = new Room(length2, width2);
            colorRoof = color;
        }
        public double GetAreaHouse()
        {
            return bedroom.GetAreaRoom() + kitchen.GetAreaRoom();
        }
        public string GetColorRoof()
        {
            return colorRoof.Name;
        }
    }

}
