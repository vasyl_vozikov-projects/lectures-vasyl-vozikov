﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Lecture___Aggregation
{
    class Program
    {
        static void Main(string[] args)
        {
            House house1 = new House(5, 10, 3, 6, Color.Green);
            House house2 = new House(1, 10, 3, 6, Color.Brown);

            if (house1.GetAreaHouse() > house2.GetAreaHouse())
            {
                Console.WriteLine($"The first house is bigger than second house, and the color of his roof is {house1.GetColorRoof()}");
            }
            else
            {
                {
                    Console.WriteLine($"The second house is bigger than first house, and the color of his roof is {house2.GetColorRoof()}");
                }
            }
        }
    }

}
