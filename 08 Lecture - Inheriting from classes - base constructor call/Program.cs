﻿// See https://aka.ms/new-console-template for more information

namespace Base_constructor
{
    class A
    {
        private int x;
        public A(int x)
        {
            this.x = x;
        }
        public void Print()
        {
            Console.WriteLine(x);
        }
    }
    class B : A
    {
        public B(int x) : base(x)   
        {

        }
    }
    class Program
    {
        static void Main(string[] args)
        {
            B b = new B(4);
            b.Print();
        }
    }
}