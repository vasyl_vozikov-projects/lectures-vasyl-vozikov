﻿// See https://aka.ms/new-console-template for more information

//DayOfWeek day = DateTime.Now.DayOfWeek;
//Console.WriteLine($"Today is {day}");

//switch (day)
//{
//    case DayOfWeek.Monday:
//    case DayOfWeek.Thursday:
//        Console.WriteLine("Today we are listening to a lecture");
//        break;
//    case DayOfWeek.Tuesday:
//    case DayOfWeek.Wednesday:
//    case DayOfWeek.Friday:
//        Console.WriteLine("Today we are doing homework");
//        break;
//    default:
//        Console.WriteLine("Today we are having rest and walk");
//        break;
//}

namespace Enum
{
    enum Staff_Position
    {
        Junior_Tester,
        QA,
        Senior_QA,
        QA_Lead
    }

    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Career plan");

            Staff_Position sp = Staff_Position.Junior_Tester;
            Console.WriteLine($"Primary position: { sp}");
            sp++;

            Console.WriteLine($"Next position is: {sp}");

            Console.WriteLine();

            Console.WriteLine("Salary growth");
            int salary = 400;

            for (Staff_Position p = Staff_Position.Junior_Tester; p <= Staff_Position.QA_Lead; p++)
            {
                Console.WriteLine($"Position: {p}, salary: {salary}");
                salary += 400;
            }
        }
    }
}