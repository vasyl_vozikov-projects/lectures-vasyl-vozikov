﻿class Program
{
    delegate void MyDelegate(string str);
    static void Method(string str)
    {
        Console.WriteLine(str);
    }
    static void Main()
    {
        MyDelegate del1 = new MyDelegate(Method);
        del1("Hello EPAM!!!");

        MyDelegate del2 = Method;
        del1("Hello world!!!");
    }
}