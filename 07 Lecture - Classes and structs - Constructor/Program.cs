﻿// See https://aka.ms/new-console-template for more information

namespace OOP_3
{
    class Person
    {
        public string FirstName;
        public string LastName;
        public int Age;

        // Constructor with parameters
        public Person(string FirstName, string LastName, int Age)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Age = Age;
        }

        // Constructor to create 19 yo
        public Person(string FirstName, string LastName)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Age = 19;
        }

        // Parameterless constructor
        public Person()
        {
            this.FirstName = "Ivan";
            this.LastName = "Ivanov";
            this.Age = 89;
        }

        // Method for obtaining all info about a person
        public void AboutMe()
        {
            Console.WriteLine($"My name is {FirstName} {LastName}, I'm {Age} yo");
        }
    }

    class Program
    {
        public static void Main()
        {
            Person p1 = new Person("Sasunia", "Sabad", 22);
            p1.AboutMe();

            Person p2 = new Person("Chel", "Belimbo");
            p2.AboutMe();

            Person p3 = new Person();
            p3.AboutMe();

            Console.WriteLine("Enter first name:");
            string firstname = Console.ReadLine();

            Console.WriteLine("Enter last name:");
            string lastname = Console.ReadLine();

            Console.WriteLine("Enter age:");
            int age = int.Parse(Console.ReadLine());

            Person p4 = new Person(firstname, lastname, age);
            p4.AboutMe();
        }
    }
}
