﻿class Program
{
    static void Find(List<string> list, string template, Func<string, string, bool> predicate)
    {
        foreach (var word in list)
        {
            if (predicate(word, template))
            {
                Console.WriteLine(word);
            }
        }
    }
    static void Main()
    {
        var list = new List<string>
        {
            "студент",
            "студенческий билет",
            "студентка",
            "стадион",
            "университет",
            "студень",
        };

        Console.WriteLine("All strings, that contain 'студ'");
        Find(list, "студ", (x, y) => x.Contains(y));
        Console.WriteLine();

        Console.WriteLine("All strings, that are longer, than 'студент'");
        Find(list, "студент", (x, y) => x.Length > y.Length);
        Console.WriteLine();

        Console.WriteLine("All strings, that start like 'студент'");
        Find(list, "студент", (x, y) => x[0] == y[0]);
        Console.WriteLine();
    }
}