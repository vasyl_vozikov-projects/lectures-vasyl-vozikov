﻿interface IFirst
{
    void Method1();
}
interface ISecond : IFirst
{
    void Method2();
}
class ClassDemo : ISecond
{
    // When using an interface that inherits from another interface,
    // the implementation of the methods of both interfaces is required.
    public void Method1()
    {
        Console.WriteLine("This is Method1");
    }
    public void Method2()
    {
        Console.WriteLine("This is Method1");
    }
}

public class Program
{
    static void Main(string[] args)
    {
        ClassDemo obj1 = new ClassDemo();

        Console.WriteLine("Calling object methods");
        obj1.Method1();
        obj1.Method2();
    }
}