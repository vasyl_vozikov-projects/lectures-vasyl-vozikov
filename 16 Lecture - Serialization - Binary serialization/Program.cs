﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace _16_Lecture___Serialization___Binary_serialization
{
    [Serializable]
    class Demo
    {
        int n;
        double d;
        DateTime dt;
        public Demo(int n, double d, DateTime dt)
        {
            this.n = n;
            this.d = d;
            this.dt = dt;
        }
        public override string ToString()
        {
            string s = String.Format("Field 1 = {0}, Field 2 = {1}, Field 3 = {2}", n, d, dt.ToShortDateString());
            return s;
        }
    }
    class Program
    {
        static void Main()
        {
            Demo d = new Demo(5, 2.7, DateTime.Now);

            Console.WriteLine("Original object");
            Console.WriteLine(d);

            // To start serialization, creating new file is required
            FileStream fs = new FileStream(@"C:\Demo\file.dat", FileMode.Create);
            BinaryFormatter bf = new BinaryFormatter();
            bf.Serialize(fs, d);
            fs.Close();

            // Deserialization
            fs = new FileStream(@"C:\Demo\file.dat", FileMode.Open);
            Demo d1 = null;
            d1 = (Demo)bf.Deserialize(fs);
            Console.WriteLine("Refurbished object");
            Console.WriteLine(d1);
        }
    }
}
