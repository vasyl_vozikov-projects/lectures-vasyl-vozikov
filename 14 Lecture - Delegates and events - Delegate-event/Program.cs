﻿delegate void MyDelegate();
class Button
{
    public event MyDelegate Click;      // Event must be of the type of delegate
    public void SimulateClick()
    {
        if (Click != null)
        {
            Click();
        }
    }
    public static void Handler()        // Handler method must have the same signature as the delegate
    {
        Console.WriteLine("The button was pressed");
    }
}
class Program
{
    static void Main()
    {
        Button button = new Button();
        button.Click += Button.Handler;
        button.SimulateClick();
    }
}