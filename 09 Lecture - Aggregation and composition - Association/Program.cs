﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Lecture___Aggregation_and_composition___Association
{
    // Association is used in case when object knows about existence of another object and this awareness can be mutual
    // Lifetimes of objects are not connected
    internal class Program
    {
        static void Main(string[] args)
        {
            Room hall = new Room(4, 5);
            Console.WriteLine($"Area: {hall.GetAreaRoom()}");

            Furniture sofa = new Furniture("Sofa", "Leather");
            Furniture table = new Furniture("Table", "Wood");

            hall.AddFurniture(sofa);
            hall.AddFurniture(table);
            Console.WriteLine($"List of furnitures in the room:");
            hall.ListFurniture();

            hall.TakeOutFurniture(table);
            Console.WriteLine($"List of furnitures in the room:");
            hall.ListFurniture();
        }
    }
}
