﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Lecture___Aggregation_and_composition___Association
{
    class Room
    {
        private double length;
        private double width;
        private Furniture[] furnitures;
        public Room(double length, double width)
        {
            this.length = length;
            this.width = width;
            furnitures = new Furniture[100];
        }
        public double GetAreaRoom()
        {
            return length * width;
        }
        public void AddFurniture(Furniture furniture)
        {
            for (int i = 0; i < 100; i++)
            {
                if (furnitures[i] == null)
                {
                    furnitures[i] = furniture;
                    break;
                }
            }
        }
        public void TakeOutFurniture(Furniture furniture)
        {
            for (int i = 0; i < 100; i++)
            {
                if (furnitures[i] == furniture)
                {
                    furnitures[i] = null;
                    break;
                }
            }
        }
        public void ListFurniture()
        {
            for (int i = 0; i < 100; i++)
            {
                if (furnitures[i] != null)
                {
                    Console.WriteLine($"{i+1}. {furnitures[i].Name}");
                }
                else
                {
                    break;
                }
            }
        }
    }
}
