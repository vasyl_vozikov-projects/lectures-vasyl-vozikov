﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Lecture___Aggregation_and_composition___Association
{    
    class Furniture
    {
        public string Name { get; }
        public string Material { get; }
        public Furniture(string name, string material)
        {
            Name = name;
            Material = material;
        }
    }
}
