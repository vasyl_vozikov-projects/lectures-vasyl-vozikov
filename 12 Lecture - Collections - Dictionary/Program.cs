﻿using System.Collections.Generic;   // To work with typed associative array, System.Collections.Generic namespace must be used
class Program
{
    static void Main(string[] args)
    {
        Dictionary<string, int> dict = new Dictionary<string, int>();
        dict.Add("Pascal", 1968);                           // Creating an object of any type,  
        dict.Add("C", 1972);                                // with standard properties .Key and .Value
        dict.Add("Java", 1995);
        dict.Add("C#", 2000);

        // To access an object of associative array, .Key property is used
        Console.WriteLine($"Pascal language was created in {dict["Pascal"]}");

        foreach (var x in dict)
        {
            Console.WriteLine($"The {x.Key} language was created in {x.Value}");
        }

        // Checking the presence of a key with standard method .ContainsKey
        if (dict.ContainsKey("Pascal"))                     
        {
            // To delete an object from Dictionary, standard method .Remove is used
            dict.Remove("Pascal");                                                  
        }
        foreach (var x in dict)
        {
            Console.WriteLine($"The {x.Key} language was created in {x.Value}");
        }
    }
}