﻿class Book
{
    string author;
    string name;
    public Book(string author, string name)
    {
        this.author = author;
        this.name = name;
    }
    public override string ToString()
    {
        return String.Format($"'{name}' by {author}");
    }
}
class Program
{
    static void Main()
    {
        // LIFO (Last In First Out) is a principle, which is used when working with stacks
        Stack<Book> stack = new Stack<Book>();
        stack.Push(new Book("Ian Griffiths", "Programming C# 8.0"));    // Adding objects of one type to queue with
        stack.Push(new Book("Jon Skeet", "C# in Depth"));               // standard method .Push
        stack.Push(new Book("Ian Griffiths", "Pro C#: With .Net and .Net Core"));

        Console.WriteLine($"Books in stack: {stack.Count}");
        Console.WriteLine($"The book on the top is {stack.Peek()}");

        Console.WriteLine("The order of reading books:");
        while (stack.Count > 0)
        {
            // To delete and return an object from stack, standard method .Pop is used
            Console.WriteLine(stack.Pop());
        }
        Console.WriteLine($"Books in stack: {stack.Count}");
    }
}