﻿// See https://aka.ms/new-console-template for more information

try
{
    Console.WriteLine("Enter first integer number");
    int firstValue = int.Parse(Console.ReadLine());

    Console.WriteLine("Enter second integer number");
    int secondValue = int.Parse(Console.ReadLine());

    int result = firstValue / secondValue;
    Console.WriteLine($"{firstValue} / {secondValue} = {result}");
}
catch (FormatException)
{
    Console.WriteLine("Incorrect format:");
    Console.WriteLine("Input string contains other characters");
}
catch (OverflowException)
{
    Console.WriteLine("Overflow:");
    Console.WriteLine("Input integer must be less than {0}", int.MaxValue);
}
catch (DivideByZeroException)
{
    Console.WriteLine("Division by zero is forbidden!");
}
catch (Exception e)
{
    Console.WriteLine("Exception: {0}", e.Message);
}
finally
{
    Console.WriteLine("Program is ended");
}