﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

static class MyExtension
{
    public static IEnumerable EvenItems(this IEnumerable x)
    {
        foreach (int item in x)
        {
            if (item % 2 == 0)
            {
                yield return item;
            }
        }
    }
    public static IEnumerable FindItems(this IEnumerable x, Predicate<int> f)
    {
        foreach (int y in x)
        {
            if (f(y))
            {
                yield return y;
            }
        }
    }
}
class Program
{
    static void Main()
    {
        int[] list = { 3, 6, 2, 7, 8, 5, 4 };
        Console.WriteLine("Source array");
        foreach (int item in list)
        {
            Console.Write($"{item}  ");
        }
        Console.WriteLine();

        Console.WriteLine("Using extension to display even elements");
        var data1 = list.EvenItems();
        foreach (int item in data1)
        {
            Console.Write($"{item}  ");
        }
        Console.WriteLine();

        Console.WriteLine("Using lambda expression to display even elements");
        var data2 = list.FindItems((x) => x % 2 ==0);
        foreach (int item in data2)
        {
            Console.Write($"{item}  ");
        }
        Console.WriteLine();

        Console.WriteLine("Using lambda expression to display odd elements");
        var data3 = list.FindItems((x) => x % 2 != 0);
        foreach (int item in data3)
        {
            Console.Write($"{item}  ");
        }
        Console.WriteLine();

        Console.WriteLine("Using lambda expression to display elements that are greater than 3");
        var data4 = list.FindItems((x) => x > 3);
        foreach (int item in data4)
        {
            Console.Write($"{item}  ");
        }
        Console.WriteLine();
    }
}