﻿// See https://aka.ms/new-console-template for more information

interface IFirst
{
    void Method1();                             // Interface method
}
abstract class AbstractClass : IFirst
{
    void IFirst.Method1()                       // Explicit implementation of interface method
    {
        Console.WriteLine("This is Method1 of interface");
    }
    public abstract void Method1();             // Abstract method
    public abstract void Method2();             // Abstract method
    public void Method3()                       // Non-abstract method
    {
        Console.WriteLine("This is non-abstract Method3 of abstract class");
    }
}
class MyClass : AbstractClass
{
    public override void Method1()
    {
        Console.WriteLine("This is overridden Method1 of abstract class");
    }
    public override void Method2()
    {
        Console.WriteLine("This is overridden Method2 of abstract class");
    }
}

class Program
{
    static void Main()
    {
        MyClass myClass = new MyClass();
        myClass.Method1();
        myClass.Method2();
        myClass.Method3();

        (myClass as MyClass as IFirst).Method1();
    }
}
