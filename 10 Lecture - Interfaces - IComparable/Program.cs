﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Lecture___Interfaces___IComparable
{
    class Student : IComparable
    {
        public string Name { get; set; }
        public int Mark { get; set; }
        public Student(string name, int mark)
        {
            Name = name;
            Mark = mark;
        }
        public int CompareTo(object obj)
        {
            if (obj is null)
            {
                return 1;
            }
            Student otherstudent = obj as Student;
            if (!(otherstudent is null))
            {
                if (Mark > otherstudent.Mark)
                {
                    return 1;
                }
                else if (Mark < otherstudent.Mark)
                {
                    return -1;
                }
                else
                {
                    return 0;
                }
            }
            else
            {
                throw new ArgumentException("Object is not a student!");
            }
        }
    }

    class Program
    {
        static void Main()
        {
            Student[] list = new Student[5];
            list[0] = new Student("Yakovenko", 80);
            list[1] = new Student("Petrenko", 79);
            list[2] = new Student("Antonova", 85);
            list[3] = new Student("Fedorenko", 75);
            list[4] = new Student("Bohdanov", 94);

            Console.WriteLine("Source list");
            foreach (Student student in list)
            {
                Console.WriteLine("{0,-10} {1}", student.Name, student.Mark);
            }
            
            Console.WriteLine("Sorted list");
            Array.Sort(list);
            foreach (Student student in list)
            {
                Console.WriteLine("{0,-10} {1}", student.Name, student.Mark);
            }
        }
    }
}
