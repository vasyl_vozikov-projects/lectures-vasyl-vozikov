﻿using System.Collections;
class Cat
{
    public override string ToString()
    {
        return "cat";
    }
}
class Dog
{
    public override string ToString()
    {
        return "dog";
    }
}
class Elephant
{
    public override string ToString()
    {
        return "elephant";
    }
}
public class Patients : CollectionBase
{
    public void AdmitPatient(object patient)
    {
        if (patient is Elephant)
        {
            Console.WriteLine("We don't admit elephants!");
        }
        else
        {
            List.Add(patient);
        }
    }
}
class Program
{
    static void Main()
    {
        Patients patients = new Patients();
        Cat cat = new Cat();
        Dog dog = new Dog();
        Elephant elephant = new Elephant();

        patients.AdmitPatient(cat);
        patients.AdmitPatient(dog);
        patients.AdmitPatient(elephant);

        foreach (object patient in patients)
        {
            Console.WriteLine($"Patient {patient} admitted");
        }
    }
}