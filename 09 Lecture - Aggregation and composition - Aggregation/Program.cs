﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Lecture___Aggregation_and_composition___Aggregation
{
    // Aggregation is used in case, when the lifetimes of an inner and outer objects are not equal
    // The object that is being included can exist without the outer object
    class Point
    {
        // Using autoproperties instead of fields, for protecting from changes
        private string Name { get; }
        private double X { get; }
        private double Y { get; }
        public Point(string name, double x, double y)
        {
            Name = name;
            X = x;
            Y = y;
        }
        public double Distance(Point C)
        {
            return Math.Sqrt((this.X - C.X) * (X - C.X) + (this.Y - C.Y) * (Y - C.Y));
        }
        public void Info()
        {
            Console.WriteLine($"Point {Name} has coordinates: x = {X} y = {Y}");
        }
    }
    class Segment
    {
        Point A, B;
        public Segment(Point A, Point B)
        {
            this.A = A;
            this.B = B;
        }

        // Static method for calculating the length of a segment
        // Existence of an object Segment is optional
        public static double Length(Point H, Point K)
        {
            return H.Distance(K);
        }

        // Instance method for calculating the length of a segment
        public double Length()
        {
            return A.Distance(B);
        }
    }
    class Triangle
    {
        public string Name { get; }
        Point A, B, C;
        public Triangle(string Name, Point A, Point B, Point C)
        {
            this.Name = Name;
            this.A = A;
            this.B = B;
            this.C = C;
        }
        public double Perimeter()
        {
            return Segment.Length(A, B) + Segment.Length(B, C) + Segment.Length(A, C);
        }
    }

    class Program
    {
        static void Main(string[] args)
        {
            Point A = new Point("A", 0, 0);
            Point B = new Point("B", 3, 0);
            Point C = new Point("C", 0, 4);

            A.Info();
            B.Info();
            C.Info();

            Triangle ABC = new Triangle("ABC", A, B, C);
            Console.WriteLine($"Perimeter of triangle {ABC.Name} is {ABC.Perimeter()}");

            ABC = null;     // Destruction of the object

            if (ABC == null)
            {
                Console.WriteLine("Instance doesn't exist");
            }
            else
            {
                Console.WriteLine("Instance exists");
            }

            // After object Triangle ABC have been destructed, the objects Point A, Point B and Point C are still available
            A.Info();
            B.Info();
            C.Info();

            Segment AB = new Segment(A, B);
            Console.WriteLine($"Length of segment AB is {AB.Length()}");
        }
    }
}
