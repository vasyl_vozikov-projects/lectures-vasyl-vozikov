﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Please, enter a integer from 1 to 4");

int a = int.Parse(Console.ReadLine());

switch (a)
{
    case 1:
        Console.WriteLine("You typed 1");
        break;

    case 2:
        Console.WriteLine("You typed 2");
        break;

    case 3:
        Console.WriteLine("You typed 3");
        break;

    case 4:
        Console.WriteLine("You typed 4");
        break;

    default:
        Console.WriteLine("You are mistaken!");
        break;
}
