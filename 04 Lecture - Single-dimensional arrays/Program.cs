﻿// See https://aka.ms/new-console-template for more information

// Initialization with the list of values

int[] m = { 1, 2, 3, 4, 5 };

for (int i = 0; i < m.Length; i++)
{
    Console.WriteLine($"Element at index {i} is {m[i]}");
}

// Declaring an array of 5 elements

int[] mas = new int[5];

// Entering array elements

Console.WriteLine("Enter five numbers");

for (int i = 0; i < mas.Length; i++)
{
    mas[i] = Convert.ToInt32(Console.ReadLine());
}

int sum = 0;

// Loop through array with for loop

for (int i = 0; i < mas.Length; i++)
{
    sum += mas[i];
}

Console.WriteLine($"Sum = {sum}");

// Loop through array with foreach loop

sum = 0;

foreach (int x in mas)
{
    sum += x;
}

Console.WriteLine($"Sum = {sum}");

// Finding the minimum and maximum values

int min = mas[0];
int nmin = 0;
int max = mas[0];
int nmax = 0;

for (int i = 1; i < 5; i++)
{
    if (mas[i] < min)
    {
        min = mas[i];
        nmin = i;
    }
    if (mas[i] > max)
    {
        max = mas[i];
        nmax = i;
    }
}

Console.WriteLine($"Maximum = {max}, Position = {nmax}");
Console.WriteLine($"Minimum = {min}, Position = {nmin}");
