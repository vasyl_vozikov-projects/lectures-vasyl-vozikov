﻿using System.Collections;       // To work with collection, System.Collections namespace must be used

class ThreePigs : IEnumerable   // To have the ability of using collection, IEnumerable interface must be implemented
{
    private string pig1;
    private string pig2;
    private string pig3;
    public ThreePigs(string pig1, string pig2, string pig3)
    {
        this.pig1 = pig1;
        this.pig2 = pig2;
        this.pig3 = pig3;
    }
    public string this[int index]   // To access an object of collection, indexer must be implemented
    {
        get
        {
            switch (index)
            {
                case 1:
                    return pig1;
                case 2:
                    return pig2;
                case 3:
                    return pig3;
                default:
                    return null;
            }
        }
        set
        {
            switch (index)
            {
                case 1:
                    pig1 = value; break;
                case 2:
                    pig2 = value; break;
                case 3:
                    pig3 = value; break;
            }
        }
    }
    public IEnumerator GetEnumerator()
    {
        yield return pig1;
        yield return pig2;
        yield return pig3;
    }
}
class Program
{
    static void Main()
    {
        ThreePigs pigs = new ThreePigs("Nif-Nif", "Nuf-Nuf", "Naf-Naf");

        Console.WriteLine("Pigs names:");
        foreach (var p in pigs)
        {
            Console.WriteLine(p);
        }

        // Using indexer to get information about an object of collection
        Console.WriteLine($"The name of the third pig { pigs[3]}");

        // Using indexer to set a new value to an object of collection
        pigs[3] = "Super Piggy";

        Console.WriteLine("Pigs names:");
        for (int i = 1; i <= 3; i++)
        {
            Console.WriteLine(pigs[i]);
        }
    }
}