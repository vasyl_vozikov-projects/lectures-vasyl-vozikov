﻿static class StructExtension
{
    public static bool IsPrime(this int number)
    {
        if (number <= 0)
        {
            return false;
        }
        for (int i = 2; i * i <= number; i++)
        {
            if (number % i == 0)
            {
                return false;
            }
        }
        return true;
    }
}
class Program
{
    static void Main()
    {
        for (int i = -2; i < 10; i++)
        {
            if (i.IsPrime())
            {
                Console.WriteLine($"{i} is a Prime number");
            }
            else
            {
                Console.WriteLine($"{i} is not   a Prime number");
            }
        }
        Console.WriteLine(193.IsPrime());
    }
}