﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_Lecture___Working_with_file_system___Working_with_class_Directory
{
    class Program
    {
        private static void OnChanged(object source, FileSystemEventArgs e)
        {
            WatcherChangeTypes changeType = e.ChangeType;
            Console.WriteLine("The file {0} {1}", e.FullPath, changeType.ToString());
        }
        private static void OnRenamed(object source, RenamedEventArgs e)
        {
            WatcherChangeTypes changeType = e.ChangeType;
            Console.WriteLine("The file {0} {2} to {1}",e.OldFullPath, e.FullPath, changeType.ToString());
        }
        private static void OnError(object source, ErrorEventArgs e)
        {
            Console.WriteLine("An error has occured...");
        }
        static void Main()
        {
            FileSystemWatcher watcher = new FileSystemWatcher(@"C:\Demo");
            watcher.NotifyFilter = (NotifyFilters.LastAccess | NotifyFilters.LastWrite | 
            NotifyFilters.FileName | NotifyFilters.DirectoryName);

            watcher.Changed += new FileSystemEventHandler(OnChanged);
            watcher.Created += new FileSystemEventHandler(OnChanged);
            watcher.Deleted += new FileSystemEventHandler(OnChanged);
            watcher.Renamed += new RenamedEventHandler(OnRenamed);
            watcher.Error += new ErrorEventHandler(OnError);

            watcher.EnableRaisingEvents = true;
            Console.ReadLine();
        }
    }
}
