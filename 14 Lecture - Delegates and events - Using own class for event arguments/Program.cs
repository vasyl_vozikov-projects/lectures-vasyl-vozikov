﻿class SunEventArgs : EventArgs
{
    public DateTime DT { get; private set; }
    public string Place { get; private set; }
    public string Message { get; private set; }
    public SunEventArgs(DateTime dt, string place, string message)
    {
        DT = dt;
        Place = place;
        Message = message;
    }
}
class Sun
{
    public event EventHandler<SunEventArgs> Sunrise;
    public void OnSunrise(DateTime dt, string place, string message)
    {
        Console.WriteLine($"Sun has rosen in {place}");
        Sunrise?.Invoke(this, new SunEventArgs(dt, place, message));
    }
}
class Observer
{
    string name;
    public Observer(string name)
    {
        this.name = name;
    }
    public void Handler(object sender, SunEventArgs e)
    {
        Console.WriteLine($"{name} says: {e.Message} on {e.DT.ToShortDateString()} in {e.Place}");
    }
}
class Program
{
    static void Main()
    {
        Sun sun = new Sun();

        Observer boy = new Observer("Taras");
        Observer girl = new Observer("Olena");

        sun.Sunrise += new EventHandler<SunEventArgs>(girl.Handler);
        sun.Sunrise += new EventHandler<SunEventArgs>(boy.Handler);

        sun.OnSunrise(DateTime.Now.AddDays(-3), "Lviv", "what a beautiful sunrise");
        sun.OnSunrise(DateTime.Now.AddDays(-2), "Kyiv", "what a beautiful sunrise");
        sun.OnSunrise(DateTime.Now.AddDays(-1), "Kharkiv", "what a beautiful sunrise");
    }
}