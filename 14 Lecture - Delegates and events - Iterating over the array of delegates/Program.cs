﻿delegate void Del(ref string s);
class Program
{
    public static void Method1(ref string s)
    {
        Console.WriteLine("Method1 called");
        if (s.Contains("Java"))
        {
            s = s.Replace("Java", "C#");
        }
    }
    public static void Method2(ref string s)
    {
        Console.WriteLine("Method2 called");
        s = s.ToUpper() + "!!!";
    }
    public static void Method3(ref string s)
    {
        Console.WriteLine("Method3 called");
        throw new Exception("Java is the best");
    }    
    static void Main()
    {
        string s = "I love Java";

        Del del = Method1;
        del += Method3;
        del += Method2;
        
        foreach (Del fun in del.GetInvocationList())
        {
            try
            {
                fun(ref s);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine("Exception in method " + fun.Method.Name);
            }
        }

        Console.WriteLine("Result - {0}", s);
    }
}