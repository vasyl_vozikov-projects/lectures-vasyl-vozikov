﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _15_Lecture___LINQ___Implementing_LINQ_for_a_collection
{
    public class EmployeeId
    {
        public int Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }        
    }
    public class EmployeeSalary
    {
        public int Id { get; set; }
        public int Salary { get; set; }
    }
    class Program
    {
        static void Main()
        {
            var employeeId = new List<EmployeeId>()
            {
                new EmployeeId{Id = 111, FirstName = "Petro", LastName = "Petrenko"},
                new EmployeeId{Id = 133, FirstName = "Dmytro", LastName = "Dmytrenko"},
                new EmployeeId{Id = 122, FirstName = "Anton", LastName = "Antonenko"},
                new EmployeeId{Id = 124, FirstName = "Bohdan", LastName = "Dmytrenko"}
            };

            var employeeSalary = new List<EmployeeSalary>()
            {
                new EmployeeSalary{Id = 111, Salary = 450},
                new EmployeeSalary{Id = 133, Salary = 900},
                new EmployeeSalary{Id = 122, Salary = 850},
                new EmployeeSalary{Id = 124, Salary = 900}
            };

            // Building query for combining collections
            var query1 = from empId in employeeId
                         join empSl in employeeSalary
                         on empId.Id equals empSl.Id
                         orderby empSl.Salary
                         select new
                         {
                             empId.Id,
                             empId.FirstName,
                             empId.LastName,
                             empSl.Salary,
                         };
            Console.WriteLine("Displaying combined colections:");
            foreach (var q in query1)
            {
                Console.WriteLine($"{q.Id} {q.FirstName} {q.LastName} {q.Salary}");
            }
            Console.WriteLine();
        }
    }
}
