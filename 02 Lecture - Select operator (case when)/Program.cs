﻿// See https://aka.ms/new-console-template for more information

Console.WriteLine("Please, enter rating:");

switch (int.Parse(Console.ReadLine()))
{
    case int a when a > 100:
        Console.WriteLine("The rating cannot be more than 100!");
        break;

    case int a when a < 0:
        Console.WriteLine("The rating cannot be less than 0!");
        break;

    case int a:
        Console.WriteLine($"Your rating is {a} points!");
        break;

}
