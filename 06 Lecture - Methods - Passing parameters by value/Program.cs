﻿//See https://aka.ms/new-console-template for more information

namespace Method_1
{
    // Passing parameters by value
    class Program
    {
        static void Hello(string name)
        {
            Console.WriteLine($"Hello, {name}!");
        }
        static void Sum1(int x, int y)
        {
            Console.WriteLine(x + y);
        }
        static int Sum2(int x, int y)
        {
            return x + y;
        }
        static void Main()
        {
            //Hello("Vasyl");
            //Hello("Sasha");

            //string name = Console.ReadLine();
            //Hello(name);

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            Sum1(a, b);

            int c = Sum2(a, b);

            Console.WriteLine(c);
        }
    }
}