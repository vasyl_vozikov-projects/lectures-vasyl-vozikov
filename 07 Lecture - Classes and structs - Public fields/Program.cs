﻿// See https://aka.ms/new-console-template for more information

namespace OOP_1
{
    class Person
    {
        public string FirstName;
        public string LastName;
        public int Age;
    }
    class Program
    {
        static void Main(string[] args)
        {
            Person p1 = new Person();
            p1.FirstName = "Vasyl";
            p1.LastName = "Vozikov";
            p1.Age = 29;

            Console.WriteLine($"Firstname: {p1.FirstName}, Lastname: {p1.LastName}, Age: {p1.Age}");

            Person p2 = new Person();
            p2.FirstName = "Sasha";

            Console.WriteLine($"Firstname: {p2.FirstName}, Lastname: {p2.LastName}, Age: {p2.Age}");

            Person[] people = new Person[3];
            people[0] = p1;
            people[1] = p2;

            foreach (Person person in people)
            {
                if (person != null)
                Console.WriteLine(person.FirstName);
            }
        }
    }
}