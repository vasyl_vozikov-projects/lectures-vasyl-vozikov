﻿class Car
{
    public int Speed { get; set; } = 50;
    public int SpeedUp()
    {
        Speed += 20;
        return Speed;
    }
}
static class CarExtension                       // Using of the keyword static in extension class signature is required
{
    public static int SlowDown(this Car car)    // Methods of extension class must be public static
    {
        car.Speed -= 10;
        return car.Speed;
    }
    public static int Stop(this Car car)        // The keyword this means, that method Stop() is an extension of class Car
    {
        car.Speed = 0;
        return car.Speed;
    }
}
class Program
{
    static void Main()
    {
        Car car = new Car();
        Console.WriteLine($"Start speed: {car.Speed} km/h");

        while (car.Speed < 100)
        {
            Console.WriteLine($"Speed up, speed: {car.SpeedUp()} km/h");
        }

        while (car.Speed > 10)
        {
            Console.WriteLine($"Slow down, speed: {car.SlowDown()} km/h");
        }
        Console.WriteLine($"Stop, speed: {CarExtension.Stop(car)} km/h");   
    }
}