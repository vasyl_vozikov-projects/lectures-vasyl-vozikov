﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Lecture___Interfaces___IComparable_T
{
    class Student : IComparable<Student>
    {
        public string Name { get; set; }
        public int Mark { get; set; }
        public Student(string name, int mark)
        {
            Name = name;
            Mark = mark;
        }
        public int CompareTo(Student student)
        {
            return Name.CompareTo(student.Name);    // Acsending order
         // return student.Mark.CompareTo(Mark);    // Decsending order
        }
    }

    class Program
    {
        static void Main()
        {
            Student[] list = new Student[5];
            list[0] = new Student("Yakovenko", 80);
            list[1] = new Student("Petrenko", 79);
            list[2] = new Student("Antonova", 85);
            list[3] = new Student("Fedorenko", 75);
            list[4] = new Student("Bohdanov", 94);

            Console.WriteLine("Source list");
            foreach (Student student in list)
            {
                Console.WriteLine("{0,-10} {1}", student.Name, student.Mark);
            }

            Console.WriteLine("Sorted list");
            Array.Sort(list);
            foreach (Student student in list)
            {
                Console.WriteLine("{0,-10} {1}", student.Name, student.Mark);
            }
        }
    }
}
