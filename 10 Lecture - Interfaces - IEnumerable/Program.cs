﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Lecture___Interfaces___IEnumerable
{
    class StudentNames : IEnumerable
    {
        public IEnumerator GetEnumerator()
        {
            yield return "Steve Jobs";
            yield return "Bill Gates";
            yield return "Elon Mask";
            yield return "Sergey Brin";
            yield return "Larry Page";
            yield return "Mark Zuckerberg";
        }
    }
    class Program
    {
        static void Main()
        {
            StudentNames studentNames = new StudentNames();
            foreach (var student in studentNames)
            {
                Console.WriteLine(student);
            }
        }
    }
}
