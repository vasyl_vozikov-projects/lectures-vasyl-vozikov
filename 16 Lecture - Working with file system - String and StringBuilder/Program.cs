﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_Lecture___Working_with_file_system___String_and_StringBuilder
{
    class Program
    {
        static void Main()
        {
            StringBuilder sb = new StringBuilder("String");
            String s = "String";

            sb.Append(" of char 1");         // Modifying existing object
            s += " of char 2";               // Creating new object

            Console.WriteLine(sb);
            Console.WriteLine(s);

            Console.WriteLine(sb[10]);       // Getting element with indexer
            Console.WriteLine(s[10]);

            sb[15] = '3';                    // Setting new value with indexer
          //s[15] = '3'; - Error!

            Console.WriteLine(sb);

            // StringBuilder is used, when multiple change of string is needed
            StringBuilder stringBuilder = new StringBuilder();
            String myString = "";

            DateTime startTime;
            TimeSpan interval;

            startTime = DateTime.Now;
            for (int i = 0; i < 20000; i++)
            {
                stringBuilder.Append("I like EPAM");
            }
            interval = TimeSpan.FromTicks(DateTime.Now.Ticks - startTime.Ticks);
            Console.WriteLine("Class StringBuilder:");
            Console.WriteLine("In {0} milliseconds 20,000 lines were added, and string of {1} characters was received", interval.TotalMilliseconds, stringBuilder.Length);

            startTime = DateTime.Now;
            for (int i = 0; i < 20000; i++)
            {
                myString += "I like EPAM";
            }
            interval = TimeSpan.FromTicks(DateTime.Now.Ticks - startTime.Ticks);
            Console.WriteLine("Class String:");
            Console.WriteLine("In {0} milliseconds 20,000 lines were added, and string of {1} characters was received", interval.TotalMilliseconds, myString.Length);
        }
    }
}
