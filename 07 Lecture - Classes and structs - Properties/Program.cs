﻿// See https://aka.ms/new-console-template for more information

namespace OOP_3
{
    class Person
    {
        public string FirstName;
        public string LastName;
        public int Age;

        // Constructor with parameters
        public Person(string FirstName, string LastName, int Age)
        {
            this.FirstName = FirstName;
            this.LastName = LastName;
            this.Age = Age;
        }

        public string Lastname // Property
        {
            get
            {
                return LastName; // Returns the value of the field
            }
            set
            {
                LastName = value; // Sets the value of the field
            }
        }

        public string Firstname // Property
        {
            get
            {
                return FirstName; // Returns the value of the field
            }
            set
            {
                if (value.Length > 1)
                {
                    FirstName = value; // Sets the value of the field
                }
                else
                {
                    Console.WriteLine("Name is too short!");
                }
            }
        }

        public int AGE // Property
        {
            get
            {
                return Age; // Returns the value of the field
            }
            set
            {
                if (value > 0)
                {
                    Age = value; // Sets the value of the field
                }
                else
                {
                    Age = 18; ;
                }
            }
        }
    }

    class Program
    {
        public static void Main(string[] args)
        {
            Person p = new Person("Petro", "Petrenko", 20);
            Console.WriteLine($"Name: {p.FirstName}, lastname: {p.Lastname}, age: {p.Age}");

            p.AGE++;
            Console.WriteLine($"Name: {p.FirstName}, lastname: {p.Lastname}, age: {p.Age}");

            p.FirstName = "Semen";
            Console.WriteLine($"New name: {p.FirstName}");
        }
    }
}
