﻿class Program
{
    static void Method1()
    {
        Console.WriteLine("Method1 called");
    }
    static void Method2()
    {
        Console.WriteLine("Method2 called");
    }
    static void Method3()
    {
        Console.WriteLine("Method3 called");
    }
    delegate void Del();
    static void Main()
    {
        Del del1 = new Del(Method1);
        del1 += Method2;
        del1 += Method3;
        del1();

        Console.WriteLine();
        del1 += Method2;
        del1 += Method3;
        del1();

        Console.WriteLine();
        del1 -= Method2;
        del1 -= Method2;
        del1();

        Del del2 = Method3;

        Del del3 = del1 - del2;
        del3();
    }
}