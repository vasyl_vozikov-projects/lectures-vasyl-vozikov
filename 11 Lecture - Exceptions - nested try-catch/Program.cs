﻿// See https://aka.ms/new-console-template for more information

try
{
    try
    {
        int a = int.Parse("0");
        int b = 10 / a;
    }
    catch (Exception e)
    {
        Console.WriteLine("Error in inner block");
        throw new Exception("Error handling error", e);
    }
}
catch (Exception e)
{
    Console.WriteLine("Error in outer block");
    Console.WriteLine("Message: {0}", e.Message);

    // Property InnerException gets the exception instance, that caused current exception
    if (e.InnerException != null)
    {
        Console.WriteLine("Exception instance that caused current exception: {0}", e.InnerException.Message);
        Console.WriteLine("Previous error type: {0}", e.InnerException.GetType());

    }
}