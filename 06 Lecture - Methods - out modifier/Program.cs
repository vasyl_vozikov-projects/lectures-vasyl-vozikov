﻿// See https://aka.ms/new-console-template for more information

namespace Method_3
{
    // Passing parameters with the out modifier
    class Program
    {
        static void Sum2(int x, int y, out int z)
        {
            z = x + y;
        }
        static void GetData(int a, int b, out int area, out int perim)
        {
            perim = (a + b) * 2;
            area = a * b;
        }

        static void Main(string[] args)
        {
            int x = 10;
            int y = 20;
            int z;

            Sum2(x, y, out z);
            Console.WriteLine(z);

            int sideA = 10, sideB = 20;
            GetData(sideA, sideB, out int area, out int perimeter);

            Console.WriteLine($"Area: {area}");
            Console.WriteLine($"Perimeter: {perimeter}");

            // Using out modifier with built-in method TryParse
            string s = Console.ReadLine();

             if (int.TryParse(s, out int a))
            {
                Console.WriteLine(a + 2);
            }
             else
            {
                Console.WriteLine("Enter integer number!");
            }
        }
    }
}
