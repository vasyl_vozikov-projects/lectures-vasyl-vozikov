﻿delegate double FirstDelegate(double x);         // For implementing lambda expression, delegate declaration is required
delegate double SecondDelegate(int x, int y);

class Program
{
    static void Main()
    {
        // f(x) = x + 1
        Console.WriteLine("f(x) = x + 1");
        FirstDelegate fd1 = x => x + 1;                 // Syntax of lambda expression: (parameters) => expression
        Console.WriteLine("f(2) = {0}", fd1(2));        // for multi-string expression: (parameters) => {
        Console.WriteLine();                            //                                                  expression
                                                        //                                                  expression
        // f(x) = x * x + 2 * x + 4                     //                                                  expression
        Console.WriteLine("f(x) = x * x + 2 * x + 4");  //                                              };
        fd1 = x => x * x + 2 * x + 4;
        Console.WriteLine("f(2) = {0}", fd1(2));
        Console.WriteLine();

        // f(x) = Sqrt(x)
        Console.WriteLine("f(x) = Sqrt(x)");
        fd1 = x => Math.Sqrt(x);
        Console.WriteLine("f(25) = {0}", fd1(25));
        Console.WriteLine();

        // S = 1/2 + 1/3 + ... + 1/n
        Console.WriteLine("S = 1/2 + 1/3 + ... + 1/n");
        fd1 = n =>
        {
            double s = 0;
            for (double i = 2; i < n; i++) s += 1 / i;
            return s;
        };
        Console.WriteLine("S = 1/2 + 1/3 + ... + 1/5 = {0}", fd1(5));
        Console.WriteLine();

        // f(x, y) = x * x + y * y
        Console.WriteLine("f(x, y) = x * x + y * y");
        SecondDelegate sd1 = (x, y) => x * x + y * y;
        Console.WriteLine("f(2, 5) = {0}", sd1(2, 5));
        Console.WriteLine();
    }
}