﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _09_Lecture___Aggregation_and_composition___Abstract_class
{
    abstract class Shape
    {
        public string Name { get; set; }
        protected Shape(string name)    // Protected constructor, which is available only for inheritors
        {
            Name = name;
        }
        public void Info()              // Non-abstract method in abstract class
        {
            Console.WriteLine("This is {0} and it's area: {1: #.##}", Name, Area());
        }
        public abstract double Area();
    }
    class Triangle : Shape
    {
        private double a, b, c;
        public Triangle(double a, double b, double c, string name) : base(name)
        {
            this.a = a;
            this.b = b;
            this.c = c;
        }
        public override double Area()
        {
            double p = (a + b + c) / 2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }
    }
    class Rectangle : Shape
    {
        private double a, b;
        public Rectangle(double a, double b, string name) : base(name)
        {
            this.a = a;
            this.b = b;
        }
        public override double Area()
        {
            return a * b;
        }
    }
    class Circle : Shape
    {
        private double r;
        public Circle(double r, string name) : base(name)
        {
            this.r = r;
        }
        public override double Area()
        {
            return Math.PI * r * r;
        }
    }


    class Program
    {
        static void Main(string[] args)
        {
            Shape circle = new Circle(3, "circle");     // Polymorphism
            Shape triangle = new Triangle(3, 4, 5, "triangle");
            Shape rectangle = new Rectangle(3, 4, "rectangle");

            Shape[] shapes = { circle, triangle, rectangle };
            foreach (Shape shape in shapes)
            {
                shape.Info();
            }
        }
    }
}
