﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _16_Lecture___Working_with_file_system___Working_with_class_Directory
{
    class Program
    {
        static void Main()
        {
            string currentFolder = Directory.GetCurrentDirectory();
            Console.WriteLine("Current folder: {0}", currentFolder);

            var dirName = Directory.GetDirectories(@"C:\");
            Console.WriteLine(@"List of folders in C:\");
            foreach (var dir in dirName)
            {
                Console.WriteLine(dir);
            }
            Console.WriteLine();

            var fileName = Directory.GetFiles(@"C:\");
            Console.WriteLine(@"List of files in C:\");
            foreach (var file in fileName)
            {
                Console.WriteLine(file);
            }
            Console.WriteLine();
        }
    }
}
