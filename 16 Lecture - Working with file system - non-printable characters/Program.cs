﻿Console.WriteLine("\news");                 // \n is non-printable characters, which means new line
Console.WriteLine("Content\t\t\tis here");  // \t is tabulation
Console.WriteLine(@"\news\t\t\t");          // @ is symbol, which prints all characters