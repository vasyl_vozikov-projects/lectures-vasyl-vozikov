﻿using System.Collections;       // To work with ArrayList, System.Collections namespace must be used

class Program
{
    static void Main()
    {
        ArrayList arrayList = new ArrayList();  // Creating new empty dynamic collection of type ArrayList
        arrayList.Add("Text1");
        arrayList.Add("Text2");     // Adding elements of different types to collection with standard method .Add
        arrayList.Add("Text3");
        arrayList.Add('c');
        arrayList.Add(1);
        arrayList.Add(2.5);
        arrayList.Add(true);
        arrayList.Add(new Object());
        arrayList.Add(DateTime.Now);
        arrayList.Add(new {Name = "Vasyl", LastName = "Vozikov"});  // Anonymus type

        foreach (object obj in arrayList)
        {
            Console.WriteLine(obj);
        }
        for (int i = 0; i < arrayList.Count; i++)   // Using property .Count instead of .Length to get number of elements
        {
            Console.WriteLine(arrayList[i]);
        }
    }
}