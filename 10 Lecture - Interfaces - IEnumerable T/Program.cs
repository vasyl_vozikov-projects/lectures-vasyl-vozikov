﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _01_Lecture___Interfaces___IEnumerable_T
{
    class StudentNames : IEnumerable<string>
    {
        private string[] listStudents =
        {
            "Steve Jobs",
            "Bill Gates",
            "Elon Mask",
            "Sergey Brin",
            "Larry Page",
            "Mark Zuckerberg",
        };
        public IEnumerator<string> GetEnumerator()
        {
            foreach (string s in listStudents)
            {
                yield return s;
            }
        }
        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
    class Program
    {
        static void Main()
        {
            StudentNames studentNames = new StudentNames();
            foreach (var student in studentNames)
            {
                Console.WriteLine(student);
            }
        }
    }
}
