﻿class Program
{
    static void Main()
    {
        // Standard delegate Predicate<T> accepts only parameter of type T and returns bool
        Predicate<int> IsPositive = x => x > 10;
        Console.WriteLine(IsPositive(20));
        Console.WriteLine(IsPositive(-20));

        Predicate<bool> negation = x => !x;
        Console.WriteLine(negation(true));
        Console.WriteLine(negation(false));

        Predicate<char> IsDigit = x => x >= 48 && x <= 57;
        Console.WriteLine(IsDigit('9'));
        Console.WriteLine(IsDigit(':'));
    }
}