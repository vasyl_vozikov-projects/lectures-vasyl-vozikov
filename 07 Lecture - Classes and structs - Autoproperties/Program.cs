﻿// See https://aka.ms/new-console-template for more information

namespace OOP_4
{
    class Person
    {
        public string Name { get; private set; }
        public int Age { get; set; }
        public Person()
        {
            Name = "Olena";
            Age = 30;
        }
    }
        // Autoproperty since C# 6.0
    class NewPerson
    {
        public string Name { get; } = "Oleh";
        public int Age { get; set; } = 22;
    }
    
    class Program
    {
        public static void Main(string[] args)
        {
            Person p = new Person();
            Console.WriteLine($"Name: {p.Name}, age: {p.Age}");

            p.Age++;
            Console.WriteLine($"Name: {p.Name}, age: {p.Age}");

            NewPerson p1 = new NewPerson();
            Console.WriteLine($"Name: {p1.Name}, age: {p1.Age}");

            p1.Age++;
            Console.WriteLine($"Name: {p1.Name}, age: {p1.Age}");
        }
    }
}
