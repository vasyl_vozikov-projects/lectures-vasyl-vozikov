﻿// See https://aka.ms/new-console-template for more information

class TempRecord
{
    private int[] temps = { 12, 16, 15, 16, 18, 16, 14 };
    public void PrintTemp()
    {
        foreach (int temp in temps)
        {
            Console.Write("{0}  ", temp);            
        }
        Console.WriteLine();
    }
    public int GetByIndex(int index)
    {
        return temps[index];
    }
    public void SetIndex(int index, int value)
    {
        temps[index] = value;
    }
    public int First
    {
        get
        {
            return temps[0];    
        }
        set
        {
            temps[0] = value;
        }
    }
    // Indexator 
    public int this[int index]
    {
        get
        {
            return temps[index];
        }
        set
        {
            temps[index] = value;
        }
    }
    public int Length
    {
        get { return temps.Length; }
    }
}
class Program
{
    static void Main()
    {
        TempRecord temps = new TempRecord();
        temps.PrintTemp();
        Console.WriteLine("temps[2]: {0}", temps.GetByIndex(2));

        temps.SetIndex(2, 11);
        temps.PrintTemp();

        Console.WriteLine("temps[0]: {0}", temps.First);
        temps.First = 13;
        Console.WriteLine("temps[0]: {0}", temps.First);

        Console.WriteLine("temps[0]: {0}", temps[0]);

        temps[1] = 15;
        temps[5] = 17;

        for (int i = 0; i < temps.Length; i++)
        {
            Console.Write($"{temps[i]}  ");
        }
        Console.WriteLine();
    }
}