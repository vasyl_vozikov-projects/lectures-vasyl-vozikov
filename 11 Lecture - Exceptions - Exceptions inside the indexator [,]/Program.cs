﻿// See https://aka.ms/new-console-template for more information

[Serializable]
public class ArrayException : Exception
{
    public ArrayException(String message) : base(message) { }
    public ArrayException(String message, Exception inner) : base(message, inner) { }
}
class DemoArray
{
    private int[,] myArray;
    public int LengthRows { get; }
    public int LengthColumns { get; }
    public DemoArray(int LengthRows, int LengthColumns) 
    {
        myArray = new int[LengthRows, LengthColumns];   
        this.LengthRows = LengthRows;
        this.LengthColumns = LengthColumns;
    }
    public int this[int i, int j]
    {
        get
        {
            if (i < 0 || i >= this.LengthRows || j < 0 || j >= this.LengthColumns)
            {
                throw new ArrayException("Index is incorrect");
            }
            else
            {
                return myArray[i, j];
            }
        }
        set
        {
            if (i < 0 || i >= this.LengthRows || j < 0 || j >= this.LengthColumns)
            {
                throw new ArrayException("Index is incorrect");
            }
            else if (value > 0 && value <= 100)
            {
                myArray[i,j] = value;
            }
            else
            {
                throw new ArrayException("Invalid value is assigned ");
            }
        }
    }
}

class Program
{
    static void Main()
    {
        DemoArray array = new DemoArray(3, 3);
        Random r = new Random();

        for (int i = 0; i < array.LengthRows; i++)
        {
            for (int j = 0; j < array.LengthColumns; j++)
            {
                array[i, j] = r.Next(10, 100);
                Console.Write("{0,3}", array[i, j]);
            }
            Console.WriteLine();
        }
        Console.WriteLine();

        try
        {
            Console.WriteLine(array[2, 2]);
            array[0, 1] = 199;
        }
        catch (Exception e)
        {
            Console.WriteLine(e.Message);
        }

        Console.WriteLine("New array:");
        for (int i = 0; i < array.LengthRows; i++)
        {
            for (int j = 0; j < array.LengthColumns; j++)
            {
                Console.Write("{0, 3}", array[i, j]);
            }
            Console.WriteLine();
        }
    }
}