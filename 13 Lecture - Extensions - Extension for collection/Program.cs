﻿static class ListExtension
{
    public static int SumEven(List<int> ints)
    {
        int sum = 0;
        foreach (int i in ints)
        {
            if (i % 2 == 0)
            {
                sum += i;
            }
        }
        return sum;
    }
}

class Program
{
    static void Main()
    {
        List<int> list = new List<int>();
        list.Add(1);
        list.Add(2);
        list.Add(3);
        list.Add(4);
        list.Add(5);

        Console.WriteLine($"Sum of items: {list.Sum()}");
        Console.WriteLine($"Sum of even items: {ListExtension.SumEven(list)}");
    }
}