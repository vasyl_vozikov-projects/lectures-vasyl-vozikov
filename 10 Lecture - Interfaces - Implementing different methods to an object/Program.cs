﻿interface IFirst    // Using capital "I" in name of the interface is recommended
{
    void Method();
}
interface ISecond
{
    void Method();
}
class MyClass : IFirst, ISecond
{
    public void Method()    // Implementation of common interface method
    {
        Console.WriteLine("Common overridden method called");
    }
    void IFirst.Method()    // Explicit implementation of interface method
    {
        Console.WriteLine("IFirst overridden method called");
    }
    void ISecond.Method()   // Explicit implementation of interface method
    {
        Console.WriteLine("ISecond overridden method called");
    }
}
class Program
{
    public static void Main()
    {
        MyClass obj1 = new MyClass();
        obj1.Method();

        IFirst obj2 = obj1 as IFirst;   // Creating new object of interface type
        obj2.Method();

        ISecond obj3 = new MyClass();   // Creating new object of interface type
        obj3.Method();
    }
}