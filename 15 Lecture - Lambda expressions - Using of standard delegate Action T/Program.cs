﻿delegate double MyDelegate(double x);
class Program
{
    static void Main()
    {
        // Standard delegate Action<T> is void and accepts up to 16 parameters
        // If it is necessary to pass 17 parameters to the method, anonymus method is used
        MyDelegate md = delegate(double x) { return x * x; };
        Console.WriteLine(md(5));

        Action<string> PrintString = s => Console.WriteLine(s);
        PrintString("Hello world!");

        Action<int, int> PrintSum = (x, y) => Console.WriteLine(x + y);
        PrintSum(2, 3);

        Action<string, int> AboutMe = (name, age) => Console.WriteLine($"My name is {name}, I'm {age} yo");
        AboutMe("Vasyl", 29);
    }
}